# HYDROPT @ Ocean Sciences Meeting 2020, San Diego

Code repositories for HYDROPT and Hydrolight-to-xarray parser will made available in the upcoming months. If you would like to beta test our HYDROPT framework
or have any questions please get in touch with me at tadzio.holtrop [at] vu.nl.

## References in my Ocean Sciences Presentation


Brewin, R. J. W., Dall’Olmo, G., Sathyendranath, S., & Hardman-Mountford, N. J. (2012). Particle backscattering as a function of chlorophyll and phytoplankton size structure in the open-ocean. Optics Express, 20(16), 17632. https://doi.org/10.1364/oe.20.017632

Mason, J. D., Cone, M. T., & Fry, E. S. (2016). Ultraviolet (250–550 nm) absorption spectrum of pure water. Applied Optics, 55(25), 7163. https://doi.org/10.1364/ao.55.007163

Pope, R. M., & Fry, E. S. (1997). Absorption spectrum (380–700 nm) of pure water. II. Integrating cavity measurements. Applied Optics, 36(33), 8710–8723. https://doi.org/10.1364/AO.36.008710

Tilstone, G. H., Peters, S. W. M., van der Woerd, H. J., Eleveld, M. a., Ruddick, K., Schönfeld, W., … Shutler, J. D. (2012). Variability in specific-absorption properties and their use in a semi-analytical ocean colour algorithm for MERIS in North Sea and Western English Channel Coastal Waters. Remote Sensing of Environment, 118, 320–338. https://doi.org/10.1016/j.rse.2011.11.019

Uitz, J., Huot, Y., Bruyant, F., Babin, M., & Claustre, H. (2008). Relating phytoplankton photophysiological properties to community structure on large scales. Limnology and Oceanography, 53(2), 614–630. https://doi.org/10.4319/lo.2008.53.2.0614

Van Der Woerd, H. J., & Pasterkamp, R. (2008). HYDROPT: A fast and flexible method to retrieve chlorophyll-a from multispectral satellite observations of optically complex coastal waters. Remote Sensing of Environment, 112(4), 1795–1807. https://doi.org/10.1016/j.rse.2007.09.001

